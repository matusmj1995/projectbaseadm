<?php

class DaoCliente {

    public static $instance;

    private function __construct() {}

    public static function getInstance() {
        if (!isset(self::$instance))
            self::$instance = new DaoCliente();

        return self::$instance;
    }

    public function Inserir_Cliente(PojoCliente $cliente) {
        try {
            $sql = "INSERT INTO adm_clientes (  
                adm_cli_tipo,
                adm_cli_cpf_cnpj,
                adm_cli_nome,
                adm_cli_nome_fant,
                adm_cli_dt_cad,
                adm_cli_dt_nasc,
                adm_cli_rg,
                adm_cli_grupo_id,
                adm_cli_fone_res,
                adm_cli_fone_cel1,
                adm_cli_fone_cel2,
                adm_cli_cep,
                adm_cli_log,
                adm_cli_log_cidade,
                adm_cli_log_bairro,
                adm_cli_log_uf,
                adm_cli_log_num,
                adm_cli_log_comp,
                adm_cli_email,
                adm_cli_obs       
            ) 
            VALUES (
                :cli_tipo,
                :cli_cpf_cnpj,
                :cli_nome,
                :cli_nome_fant,
                :cli_dt_cad,
                :cli_dt_nasc,
                :cli_rg,
                :cli_grupo_id,
                :cli_fone_res,
                :cli_fone_cel1,
                :cli_fone_cel2,
                :cli_cep,
                :cli_log,
                :cli_log_cidade,
                :cli_log_bairro,
                :cli_log_uf,
                :cli_log_num,
                :cli_log_comp,
                :cli_email,
                :cli_obs          
            )";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(":cli_tipo",      $cliente->getAdm_cli_tipo());
            $p_sql->bindValue(":cli_cpf_cnpj",  $cliente->getAdm_cli_cpf_cnpj());
            $p_sql->bindValue(":cli_nome",      $cliente->getAdm_cli_nome());
            $p_sql->bindValue(":cli_nome_fant", $cliente->getAdm_cli_nome_fant());
            $p_sql->bindValue(":cli_dt_cad",    $cliente->getAdm_cli_dt_cad());
            $p_sql->bindValue(":cli_dt_nasc",   $cliente->getAdm_cli_dt_nasc());
            $p_sql->bindValue(":cli_rg",        $cliente->getAdm_cli_rg());
            $p_sql->bindValue(":cli_grupo_id",  $cliente->getAdm_cli_grupo_id());
            $p_sql->bindValue(":cli_fone_res",  $cliente->getAdm_cli_fone_res());
            $p_sql->bindValue(":cli_fone_cel1", $cliente->getAdm_cli_fone_cel1());
            $p_sql->bindValue(":cli_fone_cel2", $cliente->getAdm_cli_fone_cel2());
            $p_sql->bindValue(":cli_cep",       $cliente->getAdm_cli_cep());
            $p_sql->bindValue(":cli_log",       $cliente->getAdm_cli_log());
            $p_sql->bindValue(":cli_log_cidade",$cliente->getAdm_cli_log_cidade());
            $p_sql->bindValue(":cli_log_bairro",$cliente->getAdm_cli_log_bairro());
            $p_sql->bindValue(":cli_log_uf",    $cliente->getAdm_cli_log_uf());
            $p_sql->bindValue(":cli_log_num",   $cliente->getAdm_cli_log_num());
            $p_sql->bindValue(":cli_log_comp",  $cliente->getAdm_cli_log_comp());
            $p_sql->bindValue(":cli_email",     $cliente->getAdm_cli_email());
            $p_sql->bindValue(":cli_obs",       $cliente->getAdm_cli_obs());

            if ($p_sql->execute()){ 
                $ult_id = f_ultimo_id();
                return $ult_id;
            }else{
                echo "A inserção não se realizou"; 
            }
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar_Cliente(PojoCliente $cliente) {
        try {
            $sql = "UPDATE adm_clientes set
                        soma_n1 = :n1,
                        soma_n2 = :n2,
                        soma_total = :soma
                    WHERE soma_id = :soma_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(":n1", $cliente->getN1());
            $p_sql->bindValue(":n2", $cliente->getN2());
            $p_sql->bindValue(":soma", $cliente->getSoma());
            $p_sql->bindValue(":soma_id", $cliente->getSoma_id());

            return $p_sql->execute();
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, foi gerado um LOG do mesmo, tente novamente mais tarde.";
            GeraLog::getInstance()->inserirLog("Erro: Código: " . $e->getCode() . " Mensagem: " . $e->getMessage());
        }
    }

    public function Deletar($id) {
        try {
            $sql = "DELETE FROM adm_clientes WHERE soma_id = :soma_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":soma_id", $id);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarListaClientes($ativos) {
        try {
            $sql = "SELECT * FROM adm_clientes
                WHERE adm_cli_excluido = :adm_cli_excluido";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":adm_cli_excluido", $ativos);
            $p_sql->execute();

            return $p_sql->fetchAll();

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorID($id) {
        try {
            $sql = "SELECT * FROM adm_clientes WHERE adm_cli_id = :adm_cli_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":adm_cli_id", $id);
            $p_sql->execute();
            return $this->populaCliente($p_sql->fetch(PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    private function populaCliente($row) {
            $pojo = new PojoCliente;
            $pojo->setAdm_cli_id($row['adm_cli_id']);
            $pojo->setAdm_cli_tipo($row['adm_cli_tipo']);
            $pojo->setAdm_cli_cpf_cnpj(f_formatanumero($row['adm_cli_cpf_cnpj']));
            $pojo->setAdm_cli_nome($row['adm_cli_nome']);
            $pojo->setAdm_cli_nome_fant($row['adm_cli_nome_fant']);
            $pojo->setAdm_cli_dt_cad(f_formatadata_BD($row['adm_cli_dt_cad']));
            $pojo->setAdm_cli_dt_nasc(f_formatadata_BD($row['adm_cli_dt_nasc']));
            $pojo->setAdm_cli_rg($row['adm_cli_rg']);
            $pojo->setAdm_cli_grupo_id($row['adm_cli_grupo_id']);
            $pojo->setAdm_cli_fone_res($row['adm_cli_fone_res']);
            $pojo->setAdm_cli_fone_cel1($row['adm_cli_fone_cel1']);
            $pojo->setAdm_cli_fone_cel2($row['adm_cli_fone_cel2']);
            $pojo->setAdm_cli_cep($row['adm_cli_cep']);
            $pojo->setAdm_cli_log($row['adm_cli_log']);
            $pojo->setAdm_cli_log_cidade($row['adm_cli_log_cidade']);
            $pojo->setAdm_cli_log_bairro($row['adm_cli_log_bairro']);
            $pojo->setAdm_cli_log_uf($row['adm_cli_log_uf']);
            $pojo->setAdm_cli_log_num($row['adm_cli_log_num']);
            $pojo->setAdm_cli_log_comp($row['adm_cli_log_comp']);
            $pojo->setAdm_cli_email($row['adm_cli_email']);
            $pojo->setAdm_cli_obs($row['adm_cli_obs']);
            $pojo->setAdm_cli_obs($row['adm_cli_excluido']);
            return $pojo;
        }

    }

?>